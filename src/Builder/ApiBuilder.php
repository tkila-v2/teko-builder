<?php
namespace Builder;
use Illuminate\Support\Str;

class ApiBuilder
{
    /**
     * The columns to query.
     *
     * @var \Illuminate\Database\Query\Builder
     */
    protected $table_columns;

    /**
     * Add where filters from route params.
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function applyFilters()
    {
        return function (){
            $filters = \request()->get('filters');
            if (!empty($filters)) {
                foreach ($filters as $filter => $value) {
                    if ($this->hasNamedScope($filter)){
                        $this->{$filter}($value);
                    }else {
                        $this->where($filter,'=',"{$value}");
                    }
                }
                return $this;
            }
            return $this;
        };
    }

    /**
     * Add sorting from route params.
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function applySorts()
    {
        return function (){
            $sort = \request()->get('sort');
            if (is_null($sort)) {
                return $this;
            }

            $sortFields = Str::of($sort)->explode(',');

            foreach ($sortFields as $sortField) {
                $direction = 'asc';

                if (Str::of($sortField)->startsWith('-')) {
                    $direction = 'desc';
                    $sortField = Str::of($sortField)->substr(1);
                }

                $this->orderBy($sortField, $direction);
            }
            return $this;
        };
    }

    /**
     * Add pagination from route params.
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function jsonPaginate()
    {
        return function () {
            $paginate = \request()->get('page');
            //Log::debug(print_r($this,true));
            //var_dump($paginate);
            if (is_null($paginate)) {
                return $this;
            }
            return $this->forPage(
                $page = $paginate['number'],
                $perPage = $paginate['size']
            );
        };
    }

    /**
     * Set columns to query.
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function setColumns()
    {
        return function () {
            $columns = \request()->get('fields');
            if (is_null($columns)) {
                return $this;
            }
            $fields = collect();
            foreach ($columns as $key => $value) {
                
                $collection = explode(",",$value);
                foreach ($collection as $value) {
                    $fields->push($key.".".$value);
                }
            }
            $this->table_columns = $fields->all();
            //var_dump($this->table_columns);
            return $this;
        };
    }

    /**
     * Execute the query as a "select" statement.
     *
     * @param  array|string  $columns
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getResults()
    {
        return function () {
            $columns = isset($this->table_columns) ? $this->table_columns : ['*'];
            $builder = $this->applyScopes();
    
            // If we actually found models we will also eager load any relationships that
            // have been specified as needing to be eager loaded, which will solve the
            // n+1 query issue for the developers to avoid running a lot of queries.
            if (count($models = $builder->getModels($columns)) > 0) {
                $models = $builder->eagerLoadRelations($models);
            }
    
            return $builder->getModel()->newCollection($models);
        };
    }

    /**
     * Add joins to query.
     *
     * @return Illuminate\Database\Eloquent\Builder
     */
    public function applyJoins()
    {
        return function () {
            $joins = \request()->get('include');
            if (is_null($joins)) {
                return $this;
            }
            $joins = explode(",",$joins);
            foreach ($joins as $join) {
                if ($this->hasNamedScope("join".ucfirst($join))){
                    $this->{"join".ucfirst($join)}();
                }
            }
            return $this;
        };
    }
}