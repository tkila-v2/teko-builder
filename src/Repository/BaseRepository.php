<?php
namespace Repository;

use Builder\ApiBuilder;
use Contracts\BaseRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use support\Model;

class BaseRepository implements BaseRepositoryInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
        Builder::mixin(new ApiBuilder);
    }

    public function all()
    {   try {
            return $this->model
                        ->setColumns()
                        ->applyFilters()
                        ->applySorts()
                        ->jsonPaginate()
                        ->applyJoins()
                        ->getResults();
        } catch (\Exception $th) {
            return $th->getMessage();
        }
    }

    public function get($id)
    {
        try {
            $model = $this->model->findOrFail($id);
        } catch (ModelNotFoundException $th) {
            return $th->getMessage();
        }
        return $model;
    }

    public function save(Model $model)
    {
        $model->save();
        return $model;
    }

    public function delete(Model $model)
    {
        $model->delete();
        return $model;
    }

    public function total()
    {
        $total = $this->model
                    ->applyFilters()
                    ->count();
        return intval($total);
    }

}