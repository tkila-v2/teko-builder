<?php

namespace Contracts;

use support\Model;
use support\Request;

interface BaseRepositoryInterface
{
    public function all();

    public function get($id);

    public function save(Model $model);

    public function delete(Model $model);

    public function total();
}