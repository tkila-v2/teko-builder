<?php

namespace Cache;

use Cache\Utils\KeyGenerator;
use Contracts\BaseRepositoryInterface;
use Repository\BaseRepository;
use Illuminate\Database\Eloquent\Model;
use support\Redis;

abstract class BaseCache implements BaseRepositoryInterface
{
    use KeyGenerator;

    protected $repository;
    protected $key;
    protected $cache;

    public function __construct(BaseRepository $repository, string $key)
    {
        $this->repository = $repository;
        $this->key = $key;
        $this->cache = new Redis;
    }

    public function all()
    {
        $cache_key = $this->generateCacheKey();
        if ($this->cache::hExists($this->key,$cache_key)) {
            return json_decode($this->cache::hGet($this->key,$cache_key));
        }else {
            $result = $this->repository->all();
            $this->cache::hSet($this->key,$cache_key,json_encode($result));
            return $result;
        }
    }

    public function get($id)
    {
        $cache_key = "{$this->key}.{$id}";
        if ($this->cache::hExists($this->key,$cache_key)) {
            return json_decode($this->cache::hGet($this->key,$cache_key));
        }else {
            $result = $this->repository->get($id);
            $this->cache::hSet($this->key,$cache_key,json_encode($result));
            return $result;
        }
    }

    public function save(Model $model)
    {
        $this->cache::del("{$this->key}");
        return $this->repository->save($model);
    }

    public function delete(Model $model)
    {
        $this->cache::del("{$this->key}");
        return $this->repository->delete($model);
    }

    public function total()
    {
        $cache_key = "{$this->key}.total";
        $cache_key = $cache_key.$this->filtersKey();
        if ($this->cache::hExists($this->key,$cache_key)) {
            return json_decode($this->cache::hGet($this->key,$cache_key));
        }else {
            $result = $this->repository->total();
            $this->cache::hSet($this->key,$cache_key,json_encode($result));
            return $result;
        }
    }

}