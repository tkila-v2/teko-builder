<?php

namespace Cache\Utils;

use Illuminate\Support\Str;

trait KeyGenerator {

    private $filters = [];
    private $sorts = [];
    private $paginate = [];
    private $columns = [];

    /**
     * Generate cache key.
     *
     * @return String
     */
    private function generateCacheKey()
    {
        $columns_str = $this->setColumns();
        $cache_key = "{$this->key}.{$columns_str}";
        $cache_key = $cache_key.$this->applyJoins();
        $cache_key = $cache_key.$this->filtersKey();
        $cache_key = $cache_key.$this->sortsKey();
        $cache_key = $cache_key.$this->paginateKey();
        return $cache_key;
    }

    /**
     * Set filters to cache key.
     *
     * @return String
     */
    private function filtersKey()
    {
        $filters = \request()->get('filters');
        $paginate_str_key = "";
        if (!empty($filters)) {
            $where = array();
            foreach ($filters as $filter => $value) {
                $paginate_str_key = $paginate_str_key.".{$filter}.{$value}";
                $where[] = "{$filter} LIKE '%{$value}%'";
            }
            return count($where) ? $paginate_str_key : "";
        }else {
            return "";
        }
    }

    /**
     * Set sorts to cache key.
     *
     * @return String
     */
    private function sortsKey()
    {
        $sort = \request()->get('sort');
        $sorts_str_key = ""; 
        if (!empty($sort)) {
            $sorts_array = array();
            $sortFields = Str::of($sort)->explode(',');
            foreach ($sortFields as $sortField) {
                $sorts_str_key = $sorts_str_key.".{$sortField}";
                $sorts_array[] = "{$sortField}";
            }
            return count($sorts_array) ? $sorts_str_key : "";
        }else {
            return "";
        }
    }

    /**
     * Set pagination to cache key.
     *
     * @return String
     */
    private function paginateKey()
    {
        $paginate = \request()->get('page');
        if (!empty($paginate)) {
            $currentPage = intval($paginate["number"]);
            $perPage = intval($paginate["size"]);
            return (($perPage>0) ? ".page.{$currentPage}.{$perPage}" : "");
        }else {
            return "";
        }
    }

    /**
     * Set columns to cache key.
     *
     * @return String
     */
    public function setColumns()
    {
        $columns = \request()->get('fields');
        if (is_null($columns)) {
            return '*';
        }
        $fields = collect();
        foreach ($columns as $key => $value) {
            
            $collection = explode(",",$value);
            foreach ($collection as $value) {
                $fields->push($key.".".$value);
            }
        }
        return !empty($fields->all()) ? implode('.',$fields->all()) : '*';
    }

    /**
     * Add joins to cache key.
     *
     * @return String
     */
    public function applyJoins()
    {
        $joins = \request()->get('include');
        if (is_null($joins)) {
            return "";
        }
        $joins = explode(",",$joins);
        return ".".implode(".",$joins);
    }
}